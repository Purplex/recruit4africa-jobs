

<div class="container p-md-4" >
	<div class="row">

		<div class="col">  
			<img src="images/pexels-anna-shvets.png" class="img-fluid" alt="..." >
		</div>

		<div class="col">

			<div class="card border-0 mt-md-5">
				<div class="card-body p-md-4">
					
					<h5 class="card-title section-title-1 text-wrap"> We can help you find your job easy </h5>

					<i class="fa fa-check-circle fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>



					<i class="fa fa-check fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>


					<i class="fa fa-check fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>


					<button type="button" class="section-button-1 border-0 mt-md-4">  Find Jobs  </button>


				</div>
			</div>

		</div>

	</div>
</div>






<!-- Second section -->
<div class="container p-md-4 " >
	<div class="row">

		<!-- Mobile version -->
		<div class="col d-sm-block d-md-none">  
			<img src="images/pexels-rodnae-productions.png" class="img-fluid" alt="..." >
		</div>


		<div class="col">

			<div class="card border-0 mt-md-5">
				<div class="card-body p-md-4">
					<h5 class="card-title section-title-1"> Get the best candidate for the Job  </h5>

					<i class="fa fa-check-circle fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>



					<i class="fa fa-check fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>


					<i class="fa fa-check fa-2x section-title-icon-1" aria-hidden="true"> 
						<span class="section-subtitle-1"> Keyword Research </span> 
					</i> 
					<p class="card-text section-text-1" style="margin-left: 45px;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>


					<button type="button" class="section-button-1 border-0 mt-md-4">  Find a candidate   </button>


				</div>
			</div>

		</div>



		<!-- desktop vrsion -->
		<div class="col d-none d-md-block">  
			<img src="images/pexels-rodnae-productions.png" class="img-fluid" alt="..." >
		</div>




	</div>
</div>

