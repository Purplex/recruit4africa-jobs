

<div class="container mt-1">

	<nav class="navbar navbar-expand-lg">

		<div class="container-fluid">

			<a class="navbar-brand light-yellow-site-name p-2 site-name" href="/"> <strong> RECRUIT4AFRICA </strong> </a>

			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"> <i class="fa fa-bars text-warning mt-2" aria-hidden="true"></i> </span>
			</button>

			<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				</ul>
				<div class="d-flex">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item mt-2 m-2">
							<a class="nav-link nav-url "  href="#">  <span class="fw-bold"> Find candidates </span>  </a>
						</li>
						<li class="nav-item mt-2 m-2">
							<a class="nav-link nav-url " href="#"> <span class="fw-bold"> Find jobs </span> </a>
						</li>

						@auth
						<li class="nav-item dropdown mb-3">
							<a class="nav-link light-yellow-sign-up sign-up" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								<span class="fw-bold m-3"> Account </span> <i class="fa fa-power-off " aria-hidden="true"></i>
							</a>
							<ul class="dropdown-menu navbar-dropdown" aria-labelledby="navbarDropdown">
								<li><a class="dropdown-item navbar-item-dropdown" href="{{ route('logout') }}"> Logout </a></li>
							</ul>
						</li>
						@endauth

						@guest
						<li class="nav-item mt-2 m-2">
							<a class="nav-link nav-url" href="{{ route('login') }}"> <span class="fw-bold"> Login </span> </a>
						</li>

						<li class="nav-item dropdown mb-3">
							<a class="nav-link light-yellow-sign-up sign-up" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								<span class="fw-bold m-3"> Signup for free </span> <i class="fa fa-angle-down fa-2x" aria-hidden="true"></i> 
							</a>
							<ul class="dropdown-menu navbar-dropdown" aria-labelledby="navbarDropdown">
								<li><a class="dropdown-item navbar-item-dropdown" href="{{ route('talent_registration') }}"> Find a job </a></li>
								<li><a class="dropdown-item navbar-item-dropdown" href="{{ route('employer_registration') }}"> Post a job</a></li>
							</ul>
						</li>
						@endguest


					</ul>

				</div>

			</div>
		</div>
	</nav>

</div>

